import React, { useState } from "react";
import AuthContext from "../AuthContext";


export const UserLogin = ({ children }) => {
    const [user, setUser] = useState(null);
    const login = (username, password) => {
      if (username === "codingid@example.com" && password === "password") {
        const userInfo = {
          id: 1,
          name: "Jordy kusuma",
          email: "codingid@example.com"
        };
  
        setUser(userInfo);
        return true;
      }
  
      return false;
    };
    const logout = () =>{
        setUser(null);
    }
    return (
      <AuthContext.Provider value={{ user, login, logout }}>{children}</AuthContext.Provider>
    );
};