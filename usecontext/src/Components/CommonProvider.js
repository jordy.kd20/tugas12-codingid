import React, {useState} from "react";
import Context from "../CommonContext";

export const CommonProvider = ({ children }) => {
   const [task, SetTask] = useState({
    title: "Halaman Awal",
    color: "Yellow"
   });

   const changeContext = (newTitle,newColor) =>{
    SetTask({ ...task, title: newTitle,color:newColor});
   };

    return(
        <Context.Provider value={{ task , changeContext}}>
            {children}
        </Context.Provider>
    );
};