import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./Page/Homepage";
import { Loginpage } from "./Page/Loginpage";

export const Routers = () =>{
    return(
        <Router>
            <Routes>
                <Route path='/' element={<Homepage/>}/>
                <Route path='/login' element={<Loginpage/>}/>
            </Routes>
        </Router>
    )
}