import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { CommonProvider } from './Components/CommonProvider';
import Homepage from './Page/Homepage';
import { Routers } from './Routers';
import { UserLogin } from './Components/userLogin';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(

    <UserLogin>
    <CommonProvider>
      <Routers/>
        
    </CommonProvider>
    </UserLogin>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
