import React, { useContext } from 'react'
import Context from '../CommonContext'
import { BrowserRouter as Router,useNavigate} from "react-router-dom"
import {Button, Grid} from '@mui/material'

const Homepage = () => {
    
    const {task, changeContext} = useContext(Context);
    const navigate = useNavigate();
  return (
   <Grid container spacing={2}>
    <Grid item md={12} xl={12} sx={{textAlign:'center', bgcolor:'blue', color:`${task.color}`}}>
        <h1>{task.title}</h1>
    </Grid>
    <Grid item md={12} xl={12} sx={{textAlign:'center'}}>
        <Button variant='contained' onClick={(()=>navigate('/login'))}>Login Here</Button>
    </Grid>
   </Grid>
  )
}

export default Homepage;
