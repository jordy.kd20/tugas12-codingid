import React, { useContext, useEffect, useState } from 'react'
import Context from '../CommonContext';
import { Button, Grid, Input } from '@mui/material';
import AuthContext from '../AuthContext';
import { Navigate } from 'react-router-dom';

export const Loginpage = () => {
    const {task, changeContext} = useContext(Context);
    const {user,login,logout} = useContext(AuthContext);
    const [email,SetEmail] = useState("");

    useEffect(()=>{
        changeContext("Login","Red");
    });

    
    const handleSubmit = (e) => {
        e.preventDefault();
        if (login(email, "password")) {
            alert("Berhasil");
          }
        else{
            alert("Gagal");
        }
      };
    
    const handlelogout = () =>{
        logout();
        // Navigate('/');
    }
    
  return (
    <Grid container spacing={2}>
    <Grid item md={12} xl={12} sx={{textAlign:'center', bgcolor:'blue', color:`${task.color}`}}>
        <h1>{task.title}</h1>
    </Grid>
    {user ? (
        <Grid item md={12} xl={12} sx={{textAlign:'center' }}>
         <h1>Selamat datang {user.name}</h1>
         <br/>
         <Button variant='contained' onClick={handlelogout}>Log Out</Button>
        </Grid>
        
       
    ) : (
        <Grid item md={12} xl={12} sx={{textAlign:'center'}}>
        <form onSubmit={handleSubmit}>
        <Grid item md={12} xl={12} sx={{textAlign:'center' }}>
           <Input type='email' placeholder='Email' onChange={(e) => SetEmail(e.target.value)}/>
        </Grid>
        <Grid item md={12} xl={12} sx={{textAlign:'center' }}>
           <Input type='password' placeholder='Password'/>
        </Grid>
        <Grid item md={12} xl={12} sx={{textAlign:'center' }}>
            <Button type="Submit" variant='contained'>Submit</Button>
        </Grid>
        </form>
        </Grid>
    )}
   
    
   </Grid>
  )
}
